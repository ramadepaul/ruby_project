class Article < ApplicationRecord
has_many :comments, dependent: :destroy
has_many :ratings,  dependent: :destroy
validates :title, presence: true,
                    length: { minimum: 5 }
validates :text, presence: true
def average_rating
    if self.ratings.size > 0
        self.ratings.average(:score)
    else
        'undefined'
    end
end

def lowest_rating
	self.ratings.minimum(:score)
end

def highest_rating
	self.ratings.maximum(:score)
end

def number_of_comments
	self.comments.count
end


def self.to_csv
  attributes = %w{title lowest_rating highest_rating average_rating number_of_comments}
  CSV.generate(headers: true) do |csv|
    csv << attributes
    all.each do |article|
      csv << attributes.map{ |attr| article.send(attr)}
    end
  end
end



end
